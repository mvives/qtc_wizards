%{Cpp:LicenseTemplate}\
#pragma once
@if '%{pImpl}' === 'pImpl'

#include <memory>
@endif
%{JS: QtSupport.qtIncludes([ ( '%{IncludeQObject}' )          ? 'QtCore/%{IncludeQObject}'                 : '',
                             ( '%{IncludeQWidget}' )          ? 'QtGui/%{IncludeQWidget}'                  : '',],
                           [ ( '%{IncludeQObject}' )          ? 'QtCore/%{IncludeQObject}'                 : '',
                             ( '%{IncludeQWidget}' )          ? 'QtWidgets/%{IncludeQWidget}'              : '',])}\
%{JS: Cpp.openNamespaces('%{Class}')}
@if '%{Base}'
class %{CN} : public %{Base}
@else
class %{CN}
@endif
{
@if %{isQObject}
     Q_OBJECT
@endif
public:
@if '%{Base}' === 'QObject'
    explicit %{CN}(QObject *parent = 0);
@elsif '%{Base}' === 'QWidget' || '%{Base}' === 'QMainWindow'
    explicit %{CN}(QWidget *parent = 0);
@else
    %{CN}();
@endif
@if '%{pImpl}' === 'pImpl'
    ~%{CN}();
@endif
@if %{isQObject}

signals:

public slots:
@endif
@if '%{pImpl}' === 'pImpl'

private:
    struct %{PrivName};
    std::unique_ptr<%{PrivName}> %{PrivNamePtr};
@endif
};
%{JS: Cpp.closeNamespaces('%{Class}')}\
