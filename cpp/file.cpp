%{Cpp:LicenseTemplate}\
#include "%{HdrFileName}"
@if '%{includePrecompiledHeader}' === 'includePrecompiledHeader'
#include <stdafx.h>
@endif
@if '%{emptyNamespace}' === 'emptyNamespace'

namespace {

}
@endif
%{JS: Cpp.openNamespaces('%{Class}')}
@if '%{pImpl}' === 'pImpl'
struct %{CN}::%{PrivName}
{
    %{PrivName}() = default;
};

@endif
@if '%{Base}' === 'QObject'
%{CN}::%{CN}(QObject *parent)
    : QObject(parent)
@if '%{pImpl}' === 'pImpl' && '%{cpp14}' === 'cpp14'
    , %{PrivNamePtr}(std::make_unique<%{PrivName}>())
@elsif '%{pImpl}' === 'pImpl' && '%{cpp14}' === ''
    , %{PrivNamePtr}(new %{PrivName}())
@endif
@elsif '%{Base}' === 'QWidget'
%{CN}::%{CN}(QWidget *parent)
    : %{Base}(parent)
@if '%{pImpl}' === 'pImpl' && '%{cpp14}' === 'cpp14'
    , %{PrivNamePtr}(std::make_unique<%{PrivName}>())
@elsif '%{pImpl}' === 'pImpl' && '%{cpp14}' === ''
    , %{PrivNamePtr}(new %{PrivName}())
@endif
@else
%{CN}::%{CN}()
@if '%{pImpl}' === 'pImpl' && '%{cpp14}' === 'cpp14'
    : %{PrivNamePtr}(std::make_unique<%{PrivName}>())
@elsif '%{pImpl}' === 'pImpl' && '%{cpp14}' === ''
    : %{PrivNamePtr}(new %{PrivName}())
@endif
@endif
{
}
@if '%{pImpl}' === 'pImpl'

    %{CN}::~%{CN}() = default;
@endif
%{JS: Cpp.closeNamespaces('%{Class}')}\
